name := "herokuPlayApp"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "org.postgresql" % "postgresql" % "9.2-1003-jdbc4",
  "com.github.nscala-time" %% "nscala-time" % "0.6.0"
)

play.Project.playScalaSettings
