package utils

import java.net.{HttpURLConnection, URL}
import javax.net.ssl.HttpsURLConnection
import java.io.{InputStreamReader, BufferedReader}

object Parser {
  def parseMatchesUrl(url: String) = {


    val obj = new URL(url);
    val con = obj.openConnection;

    con match {
      case httpsCon: HttpsURLConnection =>
        httpsCon.setRequestMethod("GET")
        val xml = scala.xml.XML.load(httpsCon.getInputStream)
        (xml \ "matches" \ "match").map(x => Match(x) ).toList
      case httpCon: HttpURLConnection =>
        httpCon.setRequestMethod("GET")
        val xml = scala.xml.XML.load(httpCon.getInputStream)
        (xml \ "matches" \ "match").map(x => Match(x) ).toList
      case _ => throw new Exception("Unsupported connection")
    }

  }
  
  def parseMatchUrl(url: String) = {
  	val obj = new URL(url);
    val con = obj.openConnection;

    con match {
      case httpsCon: HttpsURLConnection =>
        httpsCon.setRequestMethod("GET")
        val xml = scala.xml.XML.load(httpsCon.getInputStream)
        xml.toString
      case httpCon: HttpURLConnection =>
        httpCon.setRequestMethod("GET")
        val xml = scala.xml.XML.load(httpCon.getInputStream)
				xml.toString
      case _ => throw new Exception("Unsupported connection")
    }
  }

}

