package utils

import scala.xml.Node
import com.github.nscala_time.time.Imports._

object Match {
  def get(elem: Node, name: String) = (elem \ name).text.toLong


  def apply(elem: Node) = {
    new Match (get(elem, "match_id"), get(elem, "match_seq_num"), new DateTime(get(elem, "start_time")*1000L))
  }
}

class Match (val id: Long, val seq: Long, val time: DateTime) {
  override def toString = s"ID: $id, Sequence number: $seq, Start time: ${DateTimeFormat.forPattern("HH:mm:ss, dd MMMM yyyy").print(time)}"
}
