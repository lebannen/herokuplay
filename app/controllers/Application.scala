package controllers

import anorm._
import play.api._
import play.api.mvc._
import play.api.db.DB
import play.api.Play.current

import utils._
import com.github.nscala_time.time.Imports._


object Application extends Controller {

  def index = Action {
  	
  
  	val url = "https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?key=12C3E57E1B055024C49BC0A9BE4E5054&format=xml"
  	val matches = Parser.parseMatchesUrl(url)
    Ok(views.html.index(matches))
  }

	def findMatch(id: String) = Action {
		val url = s"http://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/v1/?key=12C3E57E1B055024C49BC0A9BE4E5054&format=xml&match_id=$id"
		
		val info = Parser.parseMatchUrl(url)
		
		Ok(views.html.findmatch(info, id))
	}
	
//  def test = Action {
//    val query =
//        """
//          |CREATE TABLE IF NOT EXISTS match (
//          |    id     integer PRIMARY KEY,
//          |    seq    integer,
//          |    time   timestamp
//          |);
//        """.stripMargin
//    DB.withConnection { implicit c =>
//    	val result: Boolean = SQL(query).execute()
//    }
//    val url = "https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?key=12C3E57E1B055024C49BC0A9BE4E5054&format=xml"
//  	val matches = Parser.parseUrl(url)
//  	
//  	matches.foreach {m =>
//  		val query = s"INSERT INTO match (id, seq) VALUES (${m.id}, ${m.seq})"
//    	DB.withConnection { implicit c =>
//    		val result: Boolean = SQL(query).execute()
//    	}			
//  	}
//  	
//    Ok(views.html.index(matches))
//  }

}
